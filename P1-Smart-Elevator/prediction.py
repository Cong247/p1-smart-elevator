import urllib.request
import json
import time
import datetime

def runPrediction():
    Time = datetime.datetime.now().time()
    hour = int(str(Time).split(":")[0])
    if hour <= 17:
        predictionList = []
        timestamp = time.strftime('%H:%M:%S')
        resultList = []
        levelList = [1, 2, 3, 4, 5, 6, 7, 8]
        # send request to get scored label from each floor
        for level in levelList:
            data = {
                "Inputs": {
                    "input1":
                        [
                            {
                                'time': timestamp,
                                'Level': level,
                                'people': "0",
                            }
                        ],
                },
                "GlobalParameters": {
                }
            }

            body = str.encode(json.dumps(data))

            url = 'https://ussouthcentral.services.azureml.net/workspaces/e9cc53ac067c43daa067b2a172d481c2/services/534cc3ad67b04022afa079c991620d62/execute?api-version=2.0&format=swagger'
            api_key = 'QO56Rh+YVZw5VGh1H6yxA8arxhhcF8/Ut0fOyoCZQaysNMFdRHy9NfEDJO35bkxK1BRMLYbU/c+KGMtNFKe7Mg=='  # Replace this with the API key for the web service
            headers = {'Content-Type': 'application/json', 'Authorization': ('Bearer ' + api_key)}

            req = urllib.request.Request(url, body, headers)

            try:
                response = urllib.request.urlopen(req)
                result = response.read()
                # adds result to result list
                resultList.append(result)

                # add prediction to prediction list
                s = json.dumps(str(result, 'utf-8'))
                result2 = json.loads(s)
                Dict = json.loads(result2)
                prediction = float(Dict["Results"]['output1'][0]['Scored Labels'])
                predictionList.append(prediction)


            except urllib.error.HTTPError as error:
                print("The request failed with status code: " + str(error.code))

                # Print the headers - they include the request ID and the timestamp, which are useful for debugging the failure
                print(error.info())
                print(json.loads(error.read().decode("utf8", 'ignore')))

        # to find the floor we should send the elevator to
        # finds the most people
        mostPeople = max(predictionList)

        # match the most people to the floor
        for i in resultList:
            s = json.dumps(str(i, 'utf-8'))
            result2 = json.loads(s)
            Dict = json.loads(result2)
            prediction = float(Dict["Results"]['output1'][0]['Scored Labels'])
            if prediction == mostPeople:
                levelToGo = int(Dict["Results"]['output1'][0]['Level'])
                # clear result and prediction list
                resultList.clear()
                predictionList.clear()
                return levelToGo

