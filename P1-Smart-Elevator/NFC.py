# PART1
import json
import time
import RPi.GPIO as GPIO
import MFRC522
import signal
import time
from firebase.firebase import FirebaseApplication
from .P1_Smart_Elevator import *
GPIO.setwarnings(False)
continue_reading = True

GPIO.cleanup()

# add in url from firebase
url = "https://smart-elevator-542bf.firebaseio.com/"
firebase = FirebaseApplication(url, None)


def FindMember(uid_value):
    result = firebase.get("/Members", uid_value)
    if result is None:
        print("User does not exist \n")
        return False
    else:
        print("Member Found")
        Name = result['Name']
        return True


def end_read(signal, frame):
    global continue_reading
    print('Ctrl-C captured, ending read')
    continue_reading = False


signal.signal(signal.SIGINT, end_read)

MIFAREReader = MFRC522.MFRC522()

print('Reading NFC tags')
print('Ctrl-C to stop')


def to_int(uid):
    value = 0
    for i in range(len(uid)):
        value = value * 256 + uid[i]
    return value


while continue_reading:
    (status, TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
    (status, uid) = MIFAREReader.MFRC522_Anticoll()

    uid_value = to_int(uid)
    if status == MIFAREReader.MI_OK:
        print('Tag uid: ' + hex(uid_value))
        time.sleep(1)
        if uid_value != 0:
            if FindMember(uid_value) == True:
                Building().to_base()