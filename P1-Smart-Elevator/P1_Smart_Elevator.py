from random import randint
import urllib.request
import json
import time
import datetime
'''WELCOME TO OUR SMART ELEVATOR'''

def runPrediction():
    predictionList = []
    timestamp = time.strftime('%H:%M:%S')
    resultList = []
    levelList = [1, 2, 3, 4, 5, 6, 7, 8]
    # send request to get scored label from each floor
    for level in levelList:
        data = {
            "Inputs": {
                "input1":
                    [
                        {
                            'time': timestamp,
                            'Level': level,
                            'people': "0",
                        }
                    ],
            },
            "GlobalParameters": {
            }
        }

        body = str.encode(json.dumps(data))

        url = 'https://ussouthcentral.services.azureml.net/workspaces/e9cc53ac067c43daa067b2a172d481c2/services/534cc3ad67b04022afa079c991620d62/execute?api-version=2.0&format=swagger'
        api_key = 'QO56Rh+YVZw5VGh1H6yxA8arxhhcF8/Ut0fOyoCZQaysNMFdRHy9NfEDJO35bkxK1BRMLYbU/c+KGMtNFKe7Mg=='  # Replace this with the API key for the web service
        headers = {'Content-Type': 'application/json', 'Authorization': ('Bearer ' + api_key)}

        req = urllib.request.Request(url, body, headers)

        try:
            response = urllib.request.urlopen(req)
            result = response.read()
            # adds result to result list
            resultList.append(result)

            # add prediction to prediction list
            s = json.dumps(str(result, 'utf-8'))
            result2 = json.loads(s)
            Dict = json.loads(result2)
            prediction = float(Dict["Results"]['output1'][0]['Scored Labels'])
            predictionList.append(prediction)


        except urllib.error.HTTPError as error:
            print("The request failed with status code: " + str(error.code))

            # Print the headers - they include the request ID and the timestamp, which are useful for debugging the failure
            print(error.info())
            print(json.loads(error.read().decode("utf8", 'ignore')))

    # to find the floor we should send the elevator to
    # finds the most people
    mostPeople = max(predictionList)

    # match the most people to the floor
    for i in resultList:
        s = json.dumps(str(i, 'utf-8'))
        result2 = json.loads(s)
        Dict = json.loads(result2)
        prediction = float(Dict["Results"]['output1'][0]['Scored Labels'])
        if prediction == mostPeople:
            levelToGo = int(Dict["Results"]['output1'][0]['Level'])
            # clear result and prediction list
            resultList.clear()
            predictionList.clear()
            return levelToGo

class Elevator():
    """An elevator class. Elevator has its position, customers inside and direction."""
    register_list = list() #people that is currently in the elevator
    current_floor = 1
    direction = 1
    door_opened = False

    def move(self):
        """Step function for elevator movement.
        In every step elevator come one floor higher or one floor lower."""
        self.current_floor += self.direction

    def exit_customers(self):
        """Function to clear all customers on their desired floor.
        Called automatically in run method of building class.
        """
        for customer in list(self.register_list):
            if customer.dst_floor == self.current_floor:
                self.cancel_customer(customer)
                self.door_opened = True
    
    def isFull(self):# this is to check if the elevator is full
        if len(self.register_list) <= 10:
            full_capacity = False
        else:
            full_capacity = True
        return full_capacity


    def register_customer(self, customer):
        """Function to add specific customer to elevator list"""
        self.register_list.append(customer)

    def cancel_customer(self, customer):
        """Function to erase specific customer from elevator list"""
        self.register_list.remove(customer)

class Customer():
    """A customer class. Customer has his/hers unique ID number,
    starting floor number and destination floor number.
    """
    start_floor = None
    dst_floor = None
    ID = None

    def __init__(self, ID, floorsNum):
        """Initializes customer and selects random value of start and destined floor.
        Destination floor differ from starting floor by default.
        """
        self.ID = ID
        self.start_floor = randint(1, floorsNum - 1) # edit this portion to prompt user for start floor and destination floor
        self.dst_floor = randint(1, floorsNum - 2)#this portion is just to ensure start floor and dst floor is different 
        if self.dst_floor >= self.start_floor:    #and both are < Num of floor in the building
            self.dst_floor += 1
    
    def setDetails(self,ID,start_floor,dst_floor): # alternate consutructor to allow user to customise customer details
        self.ID = ID
        self.start_floor = start_floor
        self.dst_floor = dst_floor


class Building():
    """A building class. Building has number of floors, list of customers outside the elevator and
    its own elevator object.
    """
    num_of_floors = None
    customer_list = list() #people waiting for the lift at lift lobby
    elevator = None

    def __init__(self):
        """Creates building class and adds list of customers to handle.
        Customer list is sorted by starting floor value for performance reasons.
        """
        self.num_of_floors = self.get_value("Please write number of floors in the building: ",\
        "Incorrect value. Number of floors should be integer higher than 1.", 2)
        customers_num = self.get_value("Please write number of customers: ",\
        "Incorrect value. Number of customers should be non-negative integer.", 0)
        for i in range(customers_num):
            self.customer_list.append(Customer(i, self.num_of_floors))
        self.customer_list = sorted(self.customer_list, key=lambda x: x.start_floor)
        self.elevator = Elevator()


    def get_value(self, message, incorrect_message, minimal_value):
        """Interface method for acquiring integer value from user, higher than minimal value."""
        val = None
        try:
            val = int(input(message))
        except ValueError:
            print("incorrect_message")
            return self.get_value(message, incorrect_message, minimal_value)
        if val < minimal_value:
            print("incorrect_message")
            return self.get_value(message, incorrect_message, minimal_value)
        else:
            return val

    def ToFifth(self):
        for i in range(self.elevator.current_floor):
            if self.elevator.current_floor > 5:
                self.elevator.direction = -1
                self.elevator.move()

            elif self.elevator.current_floor < 5:
                self.elevator.direction = +1
                self.elevator.move()

    '''Move elevator with AI when there are no customers using the lift now
     if time now is over 5pm, it will move to level 5.
     However if time is before 5pm it will move to the most crowded Level
     '''
    def MoveWithAI(self): # move the lift to the most crowded level
        Time = datetime.datetime.now().time()
        hour = int(str(Time).split(":")[0])
        if hour <= 17:
            for i in range(self.elevator.current_floor):
                if self.elevator.current_floor < runPrediction():
                    self.elevator.direction = +1
                    self.elevator.move()
                    print("Elevator has been moved to level " + str(runPrediction()))

                elif self.elevator.current_floor > runPrediction():
                    self.elevator.direction = -1
                    self.elevator.move()
                    print("Elevator has been moved to level " + str(runPrediction()))
                else:
                    break
        else:
            self.ToFifth()
            print("Elevator has been moved to level 5")


    def direction_default_strategy(self):
        """Default function of how elevator work - it starts with going to the roof of building, then
        comes back to the first floor."""
        if self.elevator.current_floor >= self.num_of_floors:
            self.elevator.direction = -1
        elif self.elevator.current_floor <= 1:
            self.elevator.direction = 1

    def to_base(self): # move the lift to first level
        for i in range(self.elevator.current_floor):
            if self.elevator.current_floor >= 1:
                self.elevator.direction = -1
                self.elevator.move()

    def enter_customers(self):
        """Function to get all customers in elevators and erase them from list that stores people waiting 
        at lift lobby."""
        for customer in list(self.customer_list): 
            if customer.start_floor == self.elevator.current_floor:
                if len(self.elevator.register_list) <= 10: #if more than 10 people in the lift
                    self.elevator.register_customer(customer) #adding people into lift/ people entering the lift
                    self.customer_list.remove(customer) #removing people from lift lobby
                    self.elevator.door_opened = True


    def run(self):
        """Core step function. Every time when called:
        - awaiting customers enter the elevator (register_customer is called)
        - the elevator direction value (+/-1) is chosen
        - elevator moves one floor up or one floor down, depending on direction value
        - any customer on his/hers floor leaves the elevator (cancel_customer is called)
        """
        self.elevator.door_opened = False
        self.elevator.exit_customers()
        if self.elevator.isFull() == False:
           self.enter_customers()
        self.direction_default_strategy()
        self.elevator.move()


    def output(self):
        """Returns total number of steps done by elevator in set strategy.
        """
        number_of_stop = 0
        total_moves = 0
        total_uneffective = 0
        while self.awaiting_customers():
            elevator_level = self.elevator.current_floor
            self.run()
            if self.elevator.door_opened:
                print("Elevator stopped at level:", elevator_level)
                number_of_stop += 1
            else:
                for i in range(len(self.customer_list)):
                    if self.customer_list[i].start_floor == self.elevator.current_floor:
                        if len(self.elevator.register_list) > 10:
                            total_uneffective += 1
            total_moves += 1
        print("Elevator not in use anymore")
        self.MoveWithAI()
        output_data = [total_moves, number_of_stop, total_uneffective]
        return output_data

    def awaiting_customers(self):
        """returns True if there is at least one customer not on his/hers destinated floor.
        Otherwise returns False. To check if there is anyone using the lift"""
        if len(self.customer_list) > 0 or len(self.elevator.register_list) > 0:
            return True
        return False


def main():
    """main function"""
    ICT_block_alternate = Building()
    num_of_customer = int(input("Enter the number of customer to register: "))
    for i in range(num_of_customer):
        start_floor = int(input("Enter Customer {0} Start Floor: ".format(i+1)))
        dst_floor = int(input("Enter Customer {0} Destination Floor: ".format(i+1)))
        print()
        cus1 = Customer(None,ICT_block_alternate.num_of_floors)
        cus1.setDetails(i,start_floor,dst_floor)
        ICT_block_alternate.customer_list.append(cus1)
    for i in range(len(ICT_block_alternate.customer_list)):
        print("Customer ID: {0}     Start floor: {1}      End Floor: {2}"
              .format(ICT_block_alternate.customer_list[i].ID,ICT_block_alternate.customer_list[i].start_floor,
                      ICT_block_alternate.customer_list[i].dst_floor))
    output_data_alternate = ICT_block_alternate.output()
    print("Total number of stops (before improvising):", output_data_alternate[2] +
          output_data_alternate[1])
    print("Total number of moves (after improvising): ", output_data_alternate[0])
    print("Total number of stops (after improvising):", output_data_alternate[1])




main()
